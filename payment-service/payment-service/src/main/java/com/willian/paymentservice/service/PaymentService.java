package com.willian.paymentservice.service;

import com.willian.paymentservice.model.Payment;

public interface PaymentService {

    void sendPayment(Payment payment);
}
